package yurasik.com.smokiealez;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;

import yurasik.com.smokiealez.fragment.StartFragment;


public class MainActivity extends ActionBarActivity {
    public final static String ADDRESSES_HTTP="addresses_http";
    private final static String TAG = MainActivity.class.getSimpleName();

    public static final String STATE_AUTO_EXECUTE = "autoexec";
    public static final String STATE_START_POWER_ON = "power";

    public static final String ACTION_STOP_MY_APPLICATION = "com.yurasik.timestop";
    private String directory="";
    public static final int REQUEST_CHOOSER_DIR  = 6761;

    public static final String MAIN_PREFERECES = "MyPref";
    public static final String INT_PAUSE = "pause";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        CloseListener closeListener = new CloseListener();
        registerReceiver(closeListener, new IntentFilter(ACTION_STOP_MY_APPLICATION));

        loadDataFromSharedPreferences();

        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        StartFragment startFragment = new StartFragment();
        fragmentManager.add(R.id.fragment, startFragment,StartFragment.class.getCanonicalName());
        fragmentManager.commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveDataInSharedPreferences();
    }


    void saveDataInSharedPreferences() {
        Log.d(TAG, "Save shared preferences");
        SharedPreferences sPref = getSharedPreferences(MAIN_PREFERECES, MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt(INT_PAUSE, ((MyApp)getApplication()).getPause());
        ed.putBoolean(STATE_AUTO_EXECUTE, ((MyApp) getApplication()).isStartOnBoot());
        ed.putBoolean(STATE_START_POWER_ON, ((MyApp)getApplication()).isStartIfCharge());
        ed.commit();
        Log.d(TAG, "Pause=" + ((MyApp) getApplication()).getPause());
    }

    void loadDataFromSharedPreferences() {
        SharedPreferences sPref = getSharedPreferences(MAIN_PREFERECES, MODE_PRIVATE);
        ((MyApp) getApplication()).setPause(sPref.getInt(INT_PAUSE, 10));
        ((MyApp) getApplication()).setStartOnBoot(sPref.getBoolean(STATE_START_POWER_ON, false));
        ((MyApp) getApplication()).setStartIfCharge(sPref.getBoolean(STATE_START_POWER_ON, false));
    }

    public class CloseListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"Stop MyActivity");
            MainActivity.this.finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult)");//: requestCode= "+requestCode+" resultCode="+resultCode);
        switch (requestCode) {
            case 137833:
                if (resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {
                    directory = data
                            .getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR);
                    Log.d(TAG, directory);

//                    settingsFragment.setTextChooseDirectory();
//                    if (isPlay) {
//                        changeDir = true;
//                    }
                } else {
                    // Nothing selected
                    Log.d(TAG, "Nothing select");
                }

                break;

        }

    }
}
