package yurasik.com.smokiealez;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;


public class MyApp extends Application {

    private final static String TAG = MyApp.class.getSimpleName();


    private int pause=10;
    private int httpAndDir=1;
    private boolean startOnBoot=false;
    private boolean startIfCharge=false;
    private String timeStart="";
    private String timeStop ="";
    private String directory="";


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "OnCreate");
    }



    public int getPause() {
        return pause;
    }

    public void setPause(int pause) {
        this.pause = pause;
    }

    public int getHttpAndDir() {
        return httpAndDir;
    }

    public void setHttpAndDir(int httpAndDir) {
        this.httpAndDir = httpAndDir;
    }

    public boolean isStartOnBoot() {
        return startOnBoot;
    }

    public void setStartOnBoot(boolean startOnBoot) {
        this.startOnBoot = startOnBoot;
    }

    public boolean isStartIfCharge() {
        return startIfCharge;
    }

    public void setStartIfCharge(boolean startIfCharge) {
        this.startIfCharge = startIfCharge;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeStop() {
        return timeStop;
    }

    public void setTimeStop(String timeStop) {
        this.timeStop = timeStop;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
