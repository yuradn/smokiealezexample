package yurasik.com.smokiealez.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;

import yurasik.com.smokiealez.R;

public class BackgroundFragment extends Fragment{
    private int addressChange=1;
    private String[] addresses;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        addresses = getResources().getStringArray(R.array.addresses);
    }

    public int getAddressChange() {
        return addressChange;
    }

    public void setAddressChange(int addressChange) {
        this.addressChange = addressChange;
    }

    public String[] getAddresses() {
        return addresses;
    }

    public void setAddresses(String[] addresses) {
        this.addresses = addresses;
    }
}
