package yurasik.com.smokiealez.fragment;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;

import java.util.Calendar;
import java.util.GregorianCalendar;

import yurasik.com.smokiealez.MyApp;
import yurasik.com.smokiealez.R;
import yurasik.com.smokiealez.receiver.TimeStopReceiver;
import yurasik.com.smokiealez.receiver.TimeUpReceiver;

public class SettingFragment extends Fragment {

    private static final String TAG = SettingFragment.class.getSimpleName();
    public static String time;
    private int pause=10;
    SeekBar seekBar;
    TextView textInterval;
    private CheckBox startOnBoot, startIfCharge;
    private static TextView textStartTimeChoise;
    private static TextView textStopTimeChoise;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pause = ((MyApp)getActivity().getApplication()).getPause();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings, null);

        textInterval = (TextView) v.findViewById(R.id.textInterval);
        textInterval.setText(""+pause);

        textStartTimeChoise = (TextView) v.findViewById(R.id.textStartTimeChoise);
        textStopTimeChoise = (TextView) v.findViewById(R.id.textStopTimeChoise);

        //Choose interval
        seekBar = (SeekBar) v.findViewById(R.id.seekBar);
        seekBar.setProgress(pause);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress < 1) {
                    seekBar.setProgress(1);
                    return;
                }
                textInterval.setText("" + progress);
                ((MyApp) getActivity().getApplication()).setPause(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        startOnBoot = (CheckBox) v.findViewById(R.id.checkAutoStart);
        startOnBoot.setChecked(((MyApp) getActivity().getApplication()).isStartOnBoot());
        startOnBoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyApp) getActivity().getApplication()).setStartOnBoot(startOnBoot.isChecked());
            }
        });

        startIfCharge = (CheckBox) v.findViewById(R.id.checkPowerOn);
        startIfCharge.setChecked(((MyApp)getActivity().getApplication()).isStartIfCharge());
        startIfCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyApp) getActivity().getApplication()).setStartIfCharge(startIfCharge.isChecked());
            }
        });

        //Choose start time
        ((TextView)v.findViewById(R.id.textStartTimeChoise)).setText(((MyApp) getActivity().getApplication()).getTimeStart());
        v.findViewById(R.id.buttonStartTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time="start";
                showTimePickerDialog();
            }
        });

        //Choose stop time
        ((TextView)v.findViewById(R.id.textStopTimeChoise)).setText(((MyApp) getActivity().getApplication()).getTimeStop());
        v.findViewById(R.id.buttonStopTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time = "stop";
                showTimePickerDialog();
            }
        });



        return v;
    }

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        public TimePickerFragment(){}

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String choose = hourOfDay + ":" + minute;
            Log.d(TAG, choose);

            Calendar cur_cal = new GregorianCalendar();
            cur_cal.setTimeInMillis(System.currentTimeMillis());

            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.DAY_OF_YEAR, cur_cal.get(Calendar.DAY_OF_YEAR));
            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cal.set(Calendar.MINUTE, minute);
            cal.set(Calendar.SECOND, cur_cal.get(Calendar.SECOND));
            cal.set(Calendar.MILLISECOND, cur_cal.get(Calendar.MILLISECOND));
            cal.set(Calendar.DATE, cur_cal.get(Calendar.DATE));
            cal.set(Calendar.MONTH, cur_cal.get(Calendar.MONTH));

            switch (time){
                case "start": {
                    ((MyApp) getActivity().getApplication()).setTimeStart(choose);
                    textStartTimeChoise.setText(choose);

                    Intent intent = new Intent(getActivity(), TimeUpReceiver.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1253, intent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                }
                break;
                case "stop": {
                    ((MyApp) getActivity().getApplication()).setTimeStop(choose);
                    textStopTimeChoise.setText(choose);

                    Intent intent = new Intent(getActivity(), TimeStopReceiver.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1254, intent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                }
                break;
            }

        }
    }
}
