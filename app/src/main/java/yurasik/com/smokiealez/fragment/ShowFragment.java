package yurasik.com.smokiealez.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;

import yurasik.com.smokiealez.MainActivity;
import yurasik.com.smokiealez.MyApp;
import yurasik.com.smokiealez.R;


public class ShowFragment extends Fragment{
    private BackgroundFragment backgroundFragment;
    private ImageView imageView;
    private int page=0;
    private int pause = 10;
    private String[] addresses;
    private ProgressBar spinner;
    private TextView title;
    private boolean pauseHandle = false;
    private Handler playStopHandler;
    private Runnable runnable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addresses=getArguments().getStringArray(MainActivity.ADDRESSES_HTTP);

        pause = ((MyApp)getActivity().getApplication()).getPause();

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getActivity());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_show,null);

        imageView = (ImageView) v.findViewById(R.id.imageView);
        spinner = (ProgressBar) v.findViewById(R.id.loading);
        title = (TextView) v.findViewById(R.id.textViewPageNumber);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show(addresses[page]);
                page++;
                if (page>addresses.length-1) page=0;
            }
        });



        playStopHandler = new Handler();
        runnable = new Runnable() {
            public void run() {
                show(addresses[page]);
                page++;
                if (page>addresses.length-1) page=0;
                if (isVisible()) playStopHandler.postDelayed(runnable, pause*1000);
            }
        };

        playStopHandler.postDelayed(runnable,0);

        return v;
    }

    private void show(String url){
        title.setText("Page "+(page+1)+"/"+addresses.length);

        ConnectivityManager connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        DisplayImageOptions options;

        if (mWifi.isConnected()) {
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.ic_empty)
                    .showImageOnFail(R.drawable.ic_error)
                    .resetViewBeforeLoading(true)
                    .cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .cacheOnDisk(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    //.displayer(new RoundedBitmapDisplayer((int) 27.5f))
                    .displayer(new FadeInBitmapDisplayer(1500))
                    .build();
        } else {
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.ic_empty)
                    .showImageOnFail(R.drawable.ic_error)
                    .resetViewBeforeLoading(true)
                    .cacheInMemory(true)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                    .cacheOnDisk(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    //.displayer(new RoundedVignetteBitmapDisplayer(10,12))
                    //.displayer(new RoundedBitmapDisplayer((int) 27.5f))
                    .displayer(new FadeInBitmapDisplayer(1500))
                    .build();
        }

        ImageLoader.getInstance().displayImage(url, imageView, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                spinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }
                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();

                spinner.setVisibility(View.GONE);
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                spinner.setMax(total);
                spinner.setProgress(current);
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        playStopHandler.removeCallbacks(runnable);
    }
}