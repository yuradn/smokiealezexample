package yurasik.com.smokiealez.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import yurasik.com.smokiealez.MainActivity;
import yurasik.com.smokiealez.MyApp;
import yurasik.com.smokiealez.R;


public class StartFragment extends Fragment {

    public final static String TAG = StartFragment.class.getSimpleName();
    public final static String LIST_ADDRESS="address";

    private int pause=10;

    SimpleAdapter sAdapter;
    ArrayList<Map<String, Object>> data;
    Map<String, Object> m;

    CheckBox checkBoxDir,checkBoxHttp;

    public StartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        checkBoxDir     = (CheckBox) v.findViewById(R.id.checkBoxDir);
        checkBoxHttp    = (CheckBox) v.findViewById(R.id.checkBoxHttp);

        Check check = new Check();

        checkBoxDir.setOnCheckedChangeListener(check);
        checkBoxHttp.setOnCheckedChangeListener(check);

        ListView lvMain = (ListView) v.findViewById(R.id.lvMain);

        final String[] addresses = getResources().getStringArray(R.array.addresses);

        data = new ArrayList<Map<String, Object>>();
        for (int i = 1; i < addresses.length; i++) {
            m = new HashMap<String, Object>();
            m.put(LIST_ADDRESS, addresses[i]);
            data.add(m);
        }

        String[] from = { LIST_ADDRESS };
        int[] to = { R.id.textViewHttpAddress };

        sAdapter = new SimpleAdapter(getActivity(), data, R.layout.item, from, to);

        lvMain.setAdapter(sAdapter);

        Button buttonStartShow = (Button) v.findViewById(R.id.buttonStartShow);
        buttonStartShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Start Show Fragment");

                FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
                ShowFragment showFragment = new ShowFragment();
                Bundle arguments = new Bundle();
                showFragment.setArguments(arguments);
                arguments.putStringArray(MainActivity.ADDRESSES_HTTP, addresses);
                fragmentManager.addToBackStack(StartFragment.class.getCanonicalName());
                fragmentManager.replace(R.id.fragment, showFragment, ShowFragment.class.getCanonicalName());
                fragmentManager.commit();
            }
        });

        Button buttonSetting = (Button) v.findViewById(R.id.buttonSetting);
        buttonSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Start Setting Fragment");

                FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
                SettingFragment settingFragment = new SettingFragment();
                Bundle arguments = new Bundle();
                arguments.putInt(MainActivity.INT_PAUSE, pause);
                settingFragment.setArguments(arguments);
                fragmentManager.addToBackStack(StartFragment.class.getCanonicalName());
                fragmentManager.replace(R.id.fragment, settingFragment, SettingFragment.class.getCanonicalName());
                fragmentManager.commit();
            }
        });

        v.findViewById(R.id.buttonChangeDirectory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the ACTION_GET_CONTENT Intent
                Intent chooserIntent = new Intent(getActivity(), DirectoryChooserActivity.class);

                // Optional: Allow users to create a new directory with a fixed name.
                chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_NEW_DIR_NAME,
                        "DirChooserSample");

                // REQUEST_DIRECTORY is a constant integer to identify the request, e.g. 0
                startActivityForResult(chooserIntent, MainActivity.REQUEST_CHOOSER_DIR);

            }
        });

        return v;
    }

    public class Check implements CompoundButton.OnCheckedChangeListener
    {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()){
                case R.id.checkBoxDir:
                    Log.d(TAG, "Dir checked: "+isChecked);
                    if (isChecked) {
                        checkBoxHttp.setOnCheckedChangeListener(null);
                        checkBoxHttp.setChecked(false);
                        checkBoxHttp.setOnCheckedChangeListener(this);
                        ((MyApp)getActivity().getApplication()).setHttpAndDir(0);
                    } else {
                        checkBoxDir.setOnCheckedChangeListener(null);
                        checkBoxDir.setChecked(true);
                        checkBoxDir.setOnCheckedChangeListener(this);
                    }
                    break;
                case R.id.checkBoxHttp:
                    Log.d(TAG, "Http checked: "+isChecked);
                    if (isChecked)
                    {
                        checkBoxDir.setOnCheckedChangeListener(null);
                        checkBoxDir.setChecked(false);
                        checkBoxDir.setOnCheckedChangeListener(this);
                        ((MyApp)getActivity().getApplication()).setHttpAndDir(1);
                    } else {
                        checkBoxHttp.setOnCheckedChangeListener(null);
                        checkBoxHttp.setChecked(true);
                        checkBoxHttp.setOnCheckedChangeListener(this);
                    }
                    break;
            }
        }
    }

}
