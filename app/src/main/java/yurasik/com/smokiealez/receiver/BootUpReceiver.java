package yurasik.com.smokiealez.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.concurrent.TimeUnit;

import yurasik.com.smokiealez.MainActivity;

public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sPref = context.getSharedPreferences(MainActivity.MAIN_PREFERECES, context.MODE_PRIVATE);
        boolean autoexec = sPref.getBoolean(MainActivity.STATE_AUTO_EXECUTE, false);
        if (autoexec) {
            Intent bootUpIntent = new Intent(context, MainActivity.class);
            bootUpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //bootUpIntent.setAction(SlideShow.ACTION_START_ON_REBOOT);
            try {
                TimeUnit.SECONDS.sleep(7L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            context.startActivity(bootUpIntent);
        }
    }
}
