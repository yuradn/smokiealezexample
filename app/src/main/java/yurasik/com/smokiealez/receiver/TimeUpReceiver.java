package yurasik.com.smokiealez.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import yurasik.com.smokiealez.MainActivity;


public class TimeUpReceiver extends BroadcastReceiver {
    private static final String TAG=TimeUpReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG,"onReceive");

        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setAction("time");

        context.startActivity(i);

    }
}
